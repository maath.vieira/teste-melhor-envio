<h4>Como executar o projeto?</h4>

<ul>
	<h4><li>Requisitos</li></h4>
		<ul>
			<li>Python3</li>
			<li>Foi utilizado o xampp com MariaDB 10.4.6 ou maior</li>
			<li>Instalar pip install mysql-connector-python</li>
		</ul>
	<h4><li>Configuração do banco de dados</li></h4>
		<ul>
			<li>Database: melhorenvio</li>
			<li>Host: localhost</li>
			<li>Username: root</li>
			<li>Password:</li>
		</ul>
	<h4><li>Execução do projeto</li></h4>
	<ol>
		<h4><li>Criar uma pasta na partição C: com o nome melhorenvio</li></h4>
		<h4><li>Abrir a pasta Utils que foi clonado junto com o projeto, </br>
		pegar o banco de dados (melhorenvio.sql) e o arquivo (logs.txt), pois foi modificado.</li></h4>
		<h4><li>Importar banco de dados mysql.</li></h4>
		<h4><li>Colocar arquivo logs.txt dentro da pasta criada chamada melhorenvio.</li></h4>
		<h4><li>Abrir CMD.</li></h4>
		<h4><li>Ir até a pasta melhor envio localizada no C: do seu computador.</li></h4>
		<h4><li>Executar no CMD dentro da pasta python main.py</li></h4>
	</ol>
	<li>Desenvolvedor: Matheus Delgado Vieira</li>
</ul>