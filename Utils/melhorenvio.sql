-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 17-Jun-2020 às 14:46
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `melhorenvio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `authenticated_entity`
--

CREATE TABLE `authenticated_entity` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `consumer_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `client_ip`
--

CREATE TABLE `client_ip` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `headers_requests`
--

CREATE TABLE `headers_requests` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `accept` varchar(100) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL,
  `usserAgent` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `headers_response`
--

CREATE TABLE `headers_response` (
  `id` int(11) NOT NULL,
  `idResponse` int(11) DEFAULT NULL,
  `contentLength` varchar(100) DEFAULT NULL,
  `via` varchar(100) DEFAULT NULL,
  `connection` varchar(100) DEFAULT NULL,
  `access_control_allow_credentials` varchar(100) DEFAULT NULL,
  `contentType` varchar(100) DEFAULT NULL,
  `server` varchar(100) DEFAULT NULL,
  `access_control_allow_origin` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `latencies`
--

CREATE TABLE `latencies` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `proxy` varchar(100) DEFAULT NULL,
  `kong` varchar(100) DEFAULT NULL,
  `request` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `paths_route`
--

CREATE TABLE `paths_route` (
  `id` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `protocols`
--

CREATE TABLE `protocols` (
  `id` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `protocol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `method` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `uri` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `querystring` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `route`
--

CREATE TABLE `route` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `hosts` varchar(100) DEFAULT NULL,
  `id_route` varchar(100) DEFAULT NULL,
  `preserveHost` varchar(100) DEFAULT NULL,
  `regexPriority` varchar(100) DEFAULT NULL,
  `stripPath` varchar(100) DEFAULT NULL,
  `updatedAtRoute` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `route_methods`
--

CREATE TABLE `route_methods` (
  `id` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `route` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `connect_timeout` varchar(100) DEFAULT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL,
  `id_service` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `port` varchar(100) DEFAULT NULL,
  `protocol` varchar(100) DEFAULT NULL,
  `read_timeout` varchar(100) DEFAULT NULL,
  `retries` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  `write_timeout` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `service_route`
--

CREATE TABLE `service_route` (
  `id` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `id_service` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `started_at`
--

CREATE TABLE `started_at` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `started` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `upstreamuri`
--

CREATE TABLE `upstreamuri` (
  `id` int(11) NOT NULL,
  `idRequests` int(11) DEFAULT NULL,
  `uri` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `authenticated_entity`
--
ALTER TABLE `authenticated_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `client_ip`
--
ALTER TABLE `client_ip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `headers_requests`
--
ALTER TABLE `headers_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `headers_response`
--
ALTER TABLE `headers_response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idResponse` (`idResponse`);

--
-- Índices para tabela `latencies`
--
ALTER TABLE `latencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `paths_route`
--
ALTER TABLE `paths_route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRoute` (`idRoute`);

--
-- Índices para tabela `protocols`
--
ALTER TABLE `protocols`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRoute` (`idRoute`);

--
-- Índices para tabela `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `route_methods`
--
ALTER TABLE `route_methods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRoute` (`idRoute`);

--
-- Índices para tabela `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `service_route`
--
ALTER TABLE `service_route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRoute` (`idRoute`);

--
-- Índices para tabela `started_at`
--
ALTER TABLE `started_at`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- Índices para tabela `upstreamuri`
--
ALTER TABLE `upstreamuri`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRequests` (`idRequests`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `authenticated_entity`
--
ALTER TABLE `authenticated_entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `client_ip`
--
ALTER TABLE `client_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `headers_requests`
--
ALTER TABLE `headers_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `headers_response`
--
ALTER TABLE `headers_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `latencies`
--
ALTER TABLE `latencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `paths_route`
--
ALTER TABLE `paths_route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `protocols`
--
ALTER TABLE `protocols`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `route`
--
ALTER TABLE `route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `route_methods`
--
ALTER TABLE `route_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `service_route`
--
ALTER TABLE `service_route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `started_at`
--
ALTER TABLE `started_at`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `upstreamuri`
--
ALTER TABLE `upstreamuri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `authenticated_entity`
--
ALTER TABLE `authenticated_entity`
  ADD CONSTRAINT `authenticated_entity_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `client_ip`
--
ALTER TABLE `client_ip`
  ADD CONSTRAINT `client_ip_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `headers_requests`
--
ALTER TABLE `headers_requests`
  ADD CONSTRAINT `headers_requests_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `headers_response`
--
ALTER TABLE `headers_response`
  ADD CONSTRAINT `headers_response_ibfk_1` FOREIGN KEY (`idResponse`) REFERENCES `response` (`id`);

--
-- Limitadores para a tabela `latencies`
--
ALTER TABLE `latencies`
  ADD CONSTRAINT `latencies_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `paths_route`
--
ALTER TABLE `paths_route`
  ADD CONSTRAINT `paths_route_ibfk_1` FOREIGN KEY (`idRoute`) REFERENCES `route` (`id`);

--
-- Limitadores para a tabela `protocols`
--
ALTER TABLE `protocols`
  ADD CONSTRAINT `protocols_ibfk_1` FOREIGN KEY (`idRoute`) REFERENCES `route` (`id`);

--
-- Limitadores para a tabela `response`
--
ALTER TABLE `response`
  ADD CONSTRAINT `response_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `route`
--
ALTER TABLE `route`
  ADD CONSTRAINT `route_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `route_methods`
--
ALTER TABLE `route_methods`
  ADD CONSTRAINT `route_methods_ibfk_1` FOREIGN KEY (`idRoute`) REFERENCES `route` (`id`);

--
-- Limitadores para a tabela `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `service_route`
--
ALTER TABLE `service_route`
  ADD CONSTRAINT `service_route_ibfk_1` FOREIGN KEY (`idRoute`) REFERENCES `route` (`id`);

--
-- Limitadores para a tabela `started_at`
--
ALTER TABLE `started_at`
  ADD CONSTRAINT `started_at_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);

--
-- Limitadores para a tabela `upstreamuri`
--
ALTER TABLE `upstreamuri`
  ADD CONSTRAINT `upstreamuri_ibfk_1` FOREIGN KEY (`idRequests`) REFERENCES `requests` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
