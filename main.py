import json
import csv
import mysql.connector
import datetime

MY_HOST = "localhost"
USERNAME = "root"
PASSWORD = ""
DATABASE = "melhorenvio"

def import_archive():
    with open('C:\melhorenvio\logs.txt') as json_file:
        mydb = mysql.connector.connect(
            host=MY_HOST,
            user=USERNAME,
            password=PASSWORD,
            database=DATABASE
        )

        query = mydb.cursor()

        method, uri, url, sizeRequest, querystring = [], [], [], [], []
        accept, host, userAgent = [], [], []
        upstreamUri = []
        statusResponse, sizeResponse = [], []
        contentLength, via, connectionResponse, accessCredentials, contentType, server, accessOrigin = [], [], [], [], [], [], []
        authenticatedEntity = []
        createdAtRoute, hostRoute, idRouteJ, preserveHost, regexPriority, stripPath, updatedAtRoute = [], [], [], [], [], [], []
        methodRoute, pathsRoute, protocolRoute, serviceRoute = [], [], [], []
        connectTimeout, createdAtService, hostService, idService, nameService = [], [], [], [], []
        pathService, port, protocolService, readTimeout, retries, updatedAtService, writeTimeout = [], [], [], [], [], [], []
        proxy, kong, requestLatencies = [], [], []
        clientIp, started = [], []

        data = json.load(json_file)

        for i in range(len(data)):
            for p2 in data[i]['request']['method']:
                method.append(p2)
            for p3 in data[i]['request']['url']:
                url.append(p3)
            for p4 in data[i]['request']['uri']:
                uri.append(p4)
            for p46 in str(data[i]['request']['size']):
                sizeRequest.append(p46)
            for p47 in data[i]['request']['querystring']:
                querystring.append(p47)

            a = query.execute("insert into requests (method, url, uri, size, querystring) values('%s', '%s', '%s', '%s', '%s')" % ( ''.join(method), ''.join(url), ''.join(uri), ''.join(sizeRequest), ''.join(querystring)))
            mydb.commit()
            idRequest = query.lastrowid

            for p5 in data[i]['request']['headers']['accept']:
                accept.append(p5)
            for p6 in data[i]['request']['headers']['host']:
                host.append(p6)
            for p7 in data[i]['request']['headers']['user-agent']:
                userAgent.append(p7)

            a = query.execute("insert into headers_requests (idRequests, accept, host, usserAgent) values('%s', '%s', '%s', '%s')" % ( idRequest, ''.join(accept), ''.join(host), ''.join(userAgent)))
            mydb.commit()

            for p8 in data[i]['upstream_uri']:
                upstreamUri.append(p8)

            a = query.execute("insert into upstreamuri (idRequests, uri) values('%s', '%s')" % ( idRequest, ''.join(upstreamUri)))
            mydb.commit()

            for p9 in str(data[i]['response']['status']):
                statusResponse.append(p9)
            for p10 in str(data[i]['response']['size']):
                sizeResponse.append(p10)

            a = query.execute("insert into response (idRequests, status, size) values('%s', '%s', '%s')" % ( idRequest, ''.join(statusResponse), ''.join(sizeResponse)))
            mydb.commit()
            idResponse = query.lastrowid

            for p11 in data[i]['response']['headers']['Content-Length']:
                contentLength.append(p11)
            for p12 in data[i]['response']['headers']['via']:
                via.append(p12)
            for p13 in data[i]['response']['headers']['Connection']:
                connectionResponse.append(p13)
            for p14 in data[i]['response']['headers']['access-control-allow-credentials']:
                accessCredentials.append(p14)
            for p15 in data[i]['response']['headers']['Content-Type']:
                contentType.append(p15)
            for p16 in data[i]['response']['headers']['server']:
                server.append(p16)
            for p48 in data[i]['response']['headers']['access-control-allow-origin']:
                accessOrigin.append(p48)

            a = query.execute("insert into headers_response (idResponse, contentLength, via, connection, access_control_allow_credentials, contentType, server, access_control_allow_origin) values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % ( idResponse, ''.join(contentLength), ''.join(via), ''.join(connectionResponse), ''.join(accessCredentials), ''.join(contentType), ''.join(server), ''.join(accessOrigin)))
            mydb.commit()

            for p17 in data[i]['authenticated_entity']['consumer_id']['uuid']:
                authenticatedEntity.append(p17)

            a = query.execute("insert into authenticated_entity (idRequests, consumer_id) values('%s', '%s')" % ( idRequest, ''.join(authenticatedEntity)))
            mydb.commit()

            for p18 in str(data[i]['route']['created_at']):
                createdAtRoute.append(p18)
            for p19 in str(data[i]['route']['hosts']):
                hostRoute.append(p19)
            for p20 in str(data[i]['route']['id']):
                idRouteJ.append(p20)
            for p21 in str(data[i]['route']['preserve_host']):
                preserveHost.append(p21)
            for p22 in str(data[i]['route']['regex_priority']):
                regexPriority.append(p22)
            for p23 in str(data[i]['route']['strip_path']):
                stripPath.append(p23)
            for p24 in str(data[i]['route']['updated_at']):
                updatedAtRoute.append(p24)

            a = query.execute("insert into route (idRequests, created_at, hosts, id_route, preserveHost, regexPriority, stripPath, updatedAtRoute) values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % ( idRequest, ''.join(createdAtRoute), ''.join(hostRoute), ''.join(idRouteJ), ''.join(preserveHost), ''.join(regexPriority), ''.join(stripPath), ''.join(updatedAtRoute)))
            mydb.commit()
            idRoute = query.lastrowid

            for p25 in data[i]['route']['methods']:
                methodRoute.append(p25)
                a = query.execute("insert into route_methods (idRoute, route) values('%s', '%s')" % ( idRoute, ''.join(methodRoute)))
                mydb.commit()
                methodRoute = []
            for p26 in data[i]['route']['paths']:
                pathsRoute.append(p26)
                a = query.execute("insert into paths_route (idRoute, path) values('%s', '%s')" % ( idRoute, ''.join(pathsRoute)))
                mydb.commit()
            for p27 in data[i]['route']['protocols']:
                protocolRoute.append(p27)
                a = query.execute("insert into protocols (idRoute, protocol) values('%s', '%s')" % ( idRoute, ''.join(protocolRoute)))
                mydb.commit()
                protocolRoute = []
            for p28 in data[i]['route']['service']['id']:
                serviceRoute.append(p28)

            a = query.execute("insert into service_route (idRoute, id_service) values('%s', '%s')" % ( idRoute, ''.join(serviceRoute)))
            mydb.commit()

            for p29 in str(data[i]['service']['connect_timeout']):
                connectTimeout.append(p29)
            for p30 in str(data[i]['service']['created_at']):
                createdAtService.append(p30)
            for p31 in data[i]['service']['host']:
                hostService.append(p31)
            for p32 in data[i]['service']['id']:
                idService.append(p32)
            for p33 in data[i]['service']['name']:
                nameService.append(p33)
            for p34 in data[i]['service']['path']:
                pathService.append(p34)
            for p35 in str(data[i]['service']['port']):
                port.append(p35)
            for p36 in data[i]['service']['protocol']:
                protocolService.append(p36)
            for p37 in str(data[i]['service']['read_timeout']):
                readTimeout.append(p37)
            for p38 in str(data[i]['service']['retries']):
                retries.append(p38)
            for p39 in str(data[i]['service']['updated_at']):
                updatedAtService.append(p39)
            for p40 in str(data[i]['service']['write_timeout']):
                writeTimeout.append(p40)

            a = query.execute("insert into service (idRequests, connect_timeout, created_at, host, id_service, name, path, port, protocol, read_timeout, retries, updated_at, write_timeout) values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s')" % (idRequest, ''.join(connectTimeout), ''.join(createdAtService), ''.join(hostService), ''.join(idService), ''.join(nameService), ''.join(pathService), ''.join(port), ''.join(protocolService), ''.join(readTimeout), ''.join(retries), ''.join(updatedAtService), ''.join(writeTimeout)))
            mydb.commit()

            for p41 in str(data[i]['latencies']['proxy']):
                proxy.append(p41)
            for p42 in str(data[i]['latencies']['kong']):
                kong.append(p42)
            for p43 in str(data[i]['latencies']['request']):
                requestLatencies.append(p43)

            a = query.execute("insert into latencies (idRequests, proxy, kong, request) values('%s', '%s', '%s', '%s')" % (idRequest, ''.join(proxy), ''.join(kong), ''.join(requestLatencies)))
            mydb.commit()

            for p44 in data[i]['client_ip']:
                clientIp.append(p44)

            a = query.execute("insert into client_ip (idRequests, ip) values('%s', '%s')" % (idRequest, ''.join(clientIp)))
            mydb.commit()

            for p45 in str(data[i]['started_at']):
                started.append(p45)

            a = query.execute("insert into started_at (idRequests, started) values('%s', '%s')" % (idRequest, ''.join(started)))
            mydb.commit()

            method, uri, url, sizeRequest, querystring = [], [], [], [], []
            accept, host, userAgent = [], [], []
            upstreamUri = []
            statusResponse, sizeResponse = [], []
            contentLength, via, connectionResponse, accessCredentials, contentType, server, accessOrigin = [], [], [], [], [], [], []
            authenticatedEntity = []
            createdAtRoute, hostRoute, idRouteJ, preserveHost, regexPriority, stripPath, updatedAtRoute = [], [], [], [], [], [], []
            methodRoute, pathsRoute, protocolRoute, serviceRoute = [], [], [], []
            connectTimeout, createdAtService, hostService, idService, nameService = [], [], [], [], []
            pathService, port, protocolService, readTimeout, retries, updatedAtService, writeTimeout = [], [], [], [], [], [], []
            proxy, kong, requestLatencies = [], [], []
            clientIp, started = [], []


def generate_service_csv():

    with open('service_'+str(datetime.datetime.now()).replace(':', '-').replace(' ','_')+'.csv', 'w', newline='') as file:
        mydb = mysql.connector.connect(
            host=MY_HOST,
            user=USERNAME,
            password=PASSWORD,
            database=DATABASE
        )
        query = mydb.cursor()

        writer = csv.writer(file)

        query.execute("select method, protocol, url, host, port, name, size, id_service, retries from service inner join requests on service.idRequests = requests.id order by id_service")
        myresult = query.fetchall()

        writer.writerow(["method", "protocol", "url", "host", "port", "name", "size", "id_service", "retries"])
        for x in myresult:
            writer.writerow(x)

        print('Arquivo criado com sucesso!')


def generate_consumer_csv():

    with open('consumer_' + str(datetime.datetime.now()).replace(':', '-').replace(' ', '_') + '.csv', 'w', newline='') as file:
        mydb = mysql.connector.connect(
            host=MY_HOST,
            user=USERNAME,
            password=PASSWORD,
            database=DATABASE
        )
        query = mydb.cursor()

        writer = csv.writer(file)

        query.execute("select consumer_id, method, url, size from authenticated_entity inner join requests on authenticated_entity.idRequests = requests.id order by consumer_id")
        myresult = query.fetchall()

        writer.writerow(["consumer_id", "method", "url", "size"])
        for x in myresult:
            writer.writerow(x)

        print('Arquivo criado com sucesso!')

def generate_time_csv():

    with open('time_' + str(datetime.datetime.now()).replace(':', '-').replace(' ', '_') + '.csv', 'w', newline='') as file:
        mydb = mysql.connector.connect(
            host=MY_HOST,
            user=USERNAME,
            password=PASSWORD,
            database=DATABASE
        )
        query = mydb.cursor()

        writer = csv.writer(file)

        query.execute("select id_service, host, name, proxy, kong, request from service inner join requests on service.idRequests = requests.id inner join latencies on latencies.idRequests = requests.id order by id_service")
        myresult = query.fetchall()

        writer.writerow(["id_service", "host", "name", "proxy", "kong", "request"])
        for x in myresult:
            writer.writerow(x)

        print('Arquivo criado com sucesso!')


def menu():
    print('*************************MENU*************************')
    print('1. Importar arquivo')
    print('2. Requisições por serviço')
    print('3. Requisições por consumidor')
    print('4. Tempo médio de request, proxy e kong por serviço')
    print('******************************************************')
    a = int(input("Coloque a opção desejada aqui: "))
    while a != 0:
        if a == 1:
            import_archive()
            break
        elif a == 2:
            generate_service_csv()
            break
        elif a == 3:
            generate_consumer_csv()
            break
        elif a == 4:
            generate_time_csv()
            break
        else:
            print('Selecione um número de 1 a 5, por favor!')
            break

menu()